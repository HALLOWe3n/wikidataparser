import os
import asyncio
import logging
import datetime

import aiohttp
import pandas as pd


def _process_csv_file(file: str, chunk_size: int) -> enumerate:
    """
    Method checks duplicates and chunks .CSV file

    """
    if file.split('.')[1] == 'csv' and chunk_size > 0:
        chunk_container = pd.read_csv(file, sep=',', usecols=[1], chunksize=chunk_size)
        # df = pd.read_csv('/Users/wizard/Dev/csvkit/std_2018_trimmed.csv', sep=',', usecols=[1])
        # without_duplicates = df.drop_duplicates()
        # without_duplicates.to_csv("/Users/wizard/Dev/csvkit/clean.csv")

        return chunk_container
    logging.error('File name is not correct, file must be .csv format')


async def _fetch(session, url: str):
    path = 'result.csv'
    async with session.get(url=url) as response:
        await asyncio.sleep(2)

        if response.status != 200:
            logging.warning(url)
            logging.warning(response.status)

        data = await response.json()
        key = list(data.get('entities'))

        qid = data.get('entities').get(*key).get('id')
        label = data.get('entities').get(*key).get('labels')
        if label:
            d = {qid: label.get('en').get('value')}
            print(d)
            df = pd.DataFrame(data=d.items())
            if not os.path.isfile(path):
                COL_NAMES = ['WIKIDATA_ID', 'WIKIDATA_LABEL']
                df.to_csv(path, index=False, header=COL_NAMES)
            else:
                df.to_csv(path, index=False, mode='a', header=False)
            return df


async def _fetch_all(session, urls_list):
    tasks = []
    index = 0
    for urls in urls_list:
        for url in urls:
            task = asyncio.create_task(_fetch(session, url))
            tasks.append(task)

        await asyncio.sleep(15)
        df = await asyncio.gather(*tasks)
        index += 1
        print(f'{index} Iteration...')
    return df


def _get_wikidata_urls(chunk_container):
    urls_list = list()
    _process_path = f"Chunk: {datetime.datetime.now()}.csv"
    url_request = "https://www.wikidata.org/w/api.php?action=wbgetentities&props=labels&ids={}&languages=en&format=json"
    for index, chunk in enumerate(chunk_container):
        data = [url_request.format(url[0].split(':')[1]) for url in chunk.values]
        urls_list.append(data)

    return urls_list


def check_time(func):
    async def inner(*args, **kwargs):
        start = datetime.datetime.now()
        await func(*args, **kwargs)
        print((datetime.datetime.now() - start))
    return inner


@check_time
async def main():
    container = _process_csv_file(file='clean.csv', chunk_size=1000)
    urls = _get_wikidata_urls(chunk_container=container)

    async with aiohttp.ClientSession() as session:
        df = await _fetch_all(session=session, urls_list=urls)
        logging.info('All data was write!') if df else logging.info('Something wrond!')

if __name__ == '__main__':
    asyncio.run(main())
